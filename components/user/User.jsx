import React from 'react'

const User = (props) => {
    const {name, age} = props
    return (
        <div>
            <hr/>
            <h2>User name: {name}</h2>
            <p>Age: {age}</p>
            <style jsx>
                {`
                    h2  {
                        color: red;
                    }
                    p {
                     color: blue;
                     font-weight: bold;
                     font-size: 1.4em;   
                    }
                `}
            </style>
        </div>
    )
}

export default User;
