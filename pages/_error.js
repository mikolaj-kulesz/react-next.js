import React from 'react'
import Link from 'next/link';
import Router from 'next/router';

const errorPage = () => {
    return (
        <div>
            <h1>Sorry, something went wrong :(</h1>
            <br/>
            <p>Do you want get back to main page? Click here:</p>
            <button onClick={() => {Router.push('/')}}>Home page</button>
        </div>
    )
}

export default errorPage
