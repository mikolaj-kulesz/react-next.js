import React from 'react'
import User from '../../components/user/User';
import fetch from 'isomorphic-unfetch'

const authIndexPage = ({stars, author}) => {
    return (
        <div>
            <h1>This is auth index page ;)</h1>
            <p>Some stars: {stars}</p>
            <i>Author: {author}</i>
            <User
                name="Mikolaj"
                age="20"/>
        </div>
    )
}

authIndexPage.getInitialProps = async ({ req }) => {
    const res = await fetch('https://api.github.com/repos/zeit/next.js')
    const json = await res.json()
    return {
        stars: json.stargazers_count,
        author: 'Mikolaj'
    }
}

export default authIndexPage
