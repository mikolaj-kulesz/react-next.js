import React from 'react'
import Link from 'next/link';
import Router from 'next/router';

const indexPage = () => {
    return (
        <div>
            <h1>Hi, this is index page!</h1>
            <br/>
            <br/>
            <Link href="/auth"><a>Auth</a></Link>
            <br/>
            <br/>
            <button onClick={() => Router.push('/auth')}>Go to Auth</button>
        </div>
    )

}

export default indexPage
